import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(
        private readonly router: Router
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // capture error
        return next.handle(request).pipe(
            tap(event => { }, err => {
                console.error(err);

                // check if error is of http error type
                if (err instanceof HttpErrorResponse) {

                    switch (err.status) {
                        // unauthorized
                        case 401: {
                            this.router.navigate(['login']);
                            break;
                        }
                    }
                }
            })
        )
    }
}

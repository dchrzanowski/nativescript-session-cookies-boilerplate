export enum ResponseStatus {
    OK = 1,
    WARNING = 0,
    ERROR = -1
}

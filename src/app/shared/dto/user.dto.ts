import { UserCamera } from './user-camera.dto';
export class User {
    id: number;
    name: string;
    email: string;
    password: string;
    role: string;
    status: string;
    stripeId: string;
    lastSubCheck: Date;
    userCameras?: UserCamera[];
}

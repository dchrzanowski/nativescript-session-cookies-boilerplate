export interface IStripeGenerateUserId {
    email: string,
    plan: string,
    source: string,
}

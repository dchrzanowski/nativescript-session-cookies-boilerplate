export interface IContactMessage {
    msg: string;
    subject: string;
    email: string;
}

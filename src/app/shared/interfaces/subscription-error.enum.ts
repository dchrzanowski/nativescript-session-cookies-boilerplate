export enum SubscriptionError {
    NONE = 'none',
    INACTIVE = 'INACTIVE',
    RETRIEVAL = 'RETRIEVAL',
    ENDED = 'ENDED',
    PROBLEM = 'PROBLEM',
    EXPIRED = 'EXPIRED',
}

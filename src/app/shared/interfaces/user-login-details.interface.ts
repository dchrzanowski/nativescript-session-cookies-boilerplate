export interface IUserLoginDetails {
    username: string,
    password: string
}

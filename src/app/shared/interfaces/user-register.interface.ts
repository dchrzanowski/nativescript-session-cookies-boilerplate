export interface IUserRegister {
    name: string,
    email: string,
    password: string,
    stripeId: string,
    subscription: string,
}

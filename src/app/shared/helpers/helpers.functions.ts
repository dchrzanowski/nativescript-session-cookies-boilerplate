import { ResponseStatus } from '../enums/response-status.enum';

/**
 * Convert a number to decimal, to a required decimal accuracy
 * @param value The value to be converted
 * @param accuracy Decimal accuracy. Amount of numbers after the dot
 */
export function toDecimal(value: number, accuracy: number) {
    if (accuracy < 0) {
        return value;
    }

    const multiplier = Math.pow(10, accuracy);

    return Math.round(value * multiplier) / multiplier;
}


/**
 * Response check for PUT/POST
 * @param response The response object
 */
export function responseCheckPostPut(response: any): ResponseStatus {

    if (!response || response.error)
        return ResponseStatus.ERROR;
    else
        return ResponseStatus.OK;
}

/**
 * Sleep for the given amount of ms
 * @param duration
 */
export function sleep(duration: number) {
    return new Promise(resolve => {
        setTimeout(resolve, duration);
    });
}

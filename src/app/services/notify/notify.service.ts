import { Injectable } from '@angular/core';
import * as Toast from 'nativescript-toast';
import { SnackBar } from "nativescript-snackbar";
import { confirm, prompt, inputType, PromptResult } from 'tns-core-modules/ui/dialogs';


@Injectable({
    providedIn: 'root'
})
export class NotifyService {
    private snackbar = new SnackBar();

    /**
     * Display a success message
     * @param msg Message to be displayed
     */
    success(msg: string) {
        this.snackbar.simple(msg, '#FFFFFF', '#28a745');
    }

    /**
     * Display an info message
     * @param msg Message to be displayed
     */
    info(msg: string) {
        this.snackbar.simple(msg, '#FFFFFF', '#17a2b8');
    }

    /**
     * Display a warning message
     * @param msg Message to be displayed
     */
    warning(msg: string) {
        this.snackbar.simple(msg, '#343a40', '#ffc107');
    }

    /**
     * Display an error message
     * @param msg Message to be displayed
     */
    error(msg: string) {
        this.snackbar.simple(msg, '#FFFFFF', '#dc3545');
    }

    /**
     * Show a Confirm Pop-Up
     * @param msg Message to display
     * @param title(optional) Title of the prompt
     * @param yes(optional) Label for the 'true' button, default 'Yes'
     * @param no(optional) Label for the 'false' button, default 'No'
     */
    async confirm(msg: string, title?: string, yes = 'Yes', no = 'No'): Promise<boolean> {
        const options = {
            title: title || '',
            message: msg,
            okButtonText: yes,
            cancelButtonText: no,
        }

        return await confirm(options);
    }

    /**
     * Show a Prompt Pop-Up
     * @param msg Message to display
     * @param title(optional) Title of the prompt
     * @param yes(optional) Label for the 'true' button, default 'Yes'
     * @param no(optional) Label for the 'false' button, default 'No'
     */
    async prompt(msg: string, title?: string, yes = 'Yes', no = 'No'): Promise<string> {
        const options = {
            title: title || '',
            message: msg,
            okButtonText: yes,
            cancelButtonText: no,
            cancelable: false,
            inputType: inputType.text,
        }
        const promptResult: PromptResult = await prompt(options);

        return promptResult.result ? promptResult.text : '';
    }

    /**
     * Shows a Toast message
     * @param msg The message to be displayed
     */
    toast(msg: string) {
        Toast.makeText(msg).show();
    }

    /**
     * Shows a Toast error (longer duration than a normal toast)
     * @param msg The message to be displayed
     */
    toastLong(msg: string) {
        Toast.makeText(msg, 'long').show();
    }

    /**
     * Something went wrong, try again.
     * Common use case convenience wrapper.
     */
    somethingWrong() {
        this.error('Something went wrong. Please try again.');
    }
}

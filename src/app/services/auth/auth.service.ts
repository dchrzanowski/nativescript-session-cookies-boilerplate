import { Injectable } from '@angular/core';
import { User } from '../../shared/dto/user.dto';
import { IUserLoginDetails } from '../../shared/interfaces/user-login-details.interface';
import { ResourceService } from '../resources/resources.service';
import { RestUserService } from '../rest/user/rest-user.service';
import { StorageService } from '../storage/storage.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    isAuthenticated: boolean = false;
    user: User;

    constructor(
        private readonly userService: RestUserService,
        private readonly storage: StorageService,
        private readonly R: ResourceService,
    ) {
        this.init();
    }

    async init() {
        try {
            await this.ping();
        } catch {
            //
        }
    }

    /**
     * Login the user
     * @param loginDetails Credentials object
     */
    login(loginDetails: IUserLoginDetails): Promise<any> {
        return new Promise((resolve, reject) => {
            this.userService.login(loginDetails)
                .subscribe((data) => {
                    this.setAsAuthenticated(data as User);
                    resolve(data);
                }, (error) => {
                    console.error(error);
                    this.isAuthenticated = false;
                    reject(error);
                });
        });
    }

    /**
     * Logout the user
     */
    logout(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.userService.logout()
                .subscribe((data) => {
                    this.setAsNotAuthenticated();
                    resolve();
                }, (error) => {
                    console.error(error);
                    reject();
                });
        })
    }

    /**
     * Ping the server and thus verify the current state of session
     * Automatically updates the current session in the local storage.
     * Upon success return the user object.
     */
    ping(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.userService.ping()
                .subscribe((data) => {
                    // user is authenticated
                    this.setAsAuthenticated(data as User);
                    resolve(data);
                }, (error) => {
                    this.setAsNotAuthenticated();
                    reject(error);
                });
        });
    }

    /**
     * Reset all auth vars
     */
    setAsNotAuthenticated() {
        this.isAuthenticated = false;
        this.user = null;
        this.storage.removeItem(this.R.str.storageKeys.user);
    }

    /**
     * Set the state as authenticated
     */
    setAsAuthenticated(user: User) {
        // mark as authenticated
        this.isAuthenticated = true;
        // set the user info
        this.user = user;
        // save user info in storage
        this.storage.setItem(this.R.str.storageKeys.user, user);
    }
}

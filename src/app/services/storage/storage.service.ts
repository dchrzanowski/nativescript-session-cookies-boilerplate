import { Injectable } from '@angular/core';
import * as ls from 'nativescript-localstorage';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor() { }

    /**
     * Set a value in the local storage
     * @param key Key to the local storage
     * @param value Value to be inserted to the local storage
     */
    setItem(key: string, value: any): void {
        ls.setItem(key, JSON.stringify(value));
    }

    /**
     * Get a value from the local storage
     * @param key Key to the local storage
     */
    getItem(key: string): any {
        return JSON.parse(ls.getItem(key));
    }

    /**
     * Remove an item from the local storage by key name
     * @param key Key to the local storage
     */
    removeItem(key: string): void {
        ls.removeItem(key);
    }
}

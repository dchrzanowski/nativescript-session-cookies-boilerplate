import { Injectable } from '@angular/core';
import { ResourceStrings } from './resources/strings.resource';
import { ResourceNums } from './resources/num.resource';
import { ResourceArrays } from './resources/array.resource';
import { ResourceSettings } from './resources/settings.resource';

@Injectable({ providedIn: 'root' })
export class ResourceService {
    str: ResourceStrings;
    num: ResourceNums;
    arr: ResourceArrays;
    settings: ResourceSettings;

    constructor() {
        this.str = new ResourceStrings();
        this.num = new ResourceNums();
        this.arr = new ResourceArrays();
        this.settings = new ResourceSettings();
    }
}

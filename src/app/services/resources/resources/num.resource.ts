export class ResourceNums {
    readonly streamCheckRetries = 30;
    readonly streamCheckDelay = 1500;
    readonly panMotorStep = 5;
    readonly tiltMotorStep = 5;
    readonly zoomMotorStep = 400;
}

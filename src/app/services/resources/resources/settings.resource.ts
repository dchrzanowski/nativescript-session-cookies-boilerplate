export class ResourceSettings {
    readonly host = {
        // url: "https://companyname.ie/",
        // urlApi: "https://companyname.ie/api/",
        // hls: "https://companyname.ie/hls/",
        url: "http://10.0.2.2:3000/",
        urlApi: "http://10.0.2.2:3000/api/",
        hls: "http://10.0.2.2/hls/"
    };
    readonly url = {
        user: {
            root: this.host.urlApi + 'user',
            register: this.host.urlApi + 'user/signup',
            login: this.host.urlApi + 'user/login',
            logout: this.host.urlApi + 'user/logout',
            recovery: this.host.urlApi + 'user/recovery',
            requestRecovery: this.host.urlApi + 'user/request-recovery',
            subscription: this.host.urlApi + 'user/subscription',
            renew: this.host.urlApi + 'user/renew',
            ping: this.host.urlApi + 'user/ping',
        },
        camera: {
            root: this.host.urlApi + 'camera/',
            pair: this.host.urlApi + 'camera/pair/',
            share: this.host.urlApi + 'camera/share/',
            cc: this.host.urlApi + 'camera/cc/',
        }
    };
    readonly companyName = "Company Name";
    readonly companyEmail = {
        contact: "contactus@companyname.ie",
        noreply: "no-reply@companyname.ie",
    };
    readonly stripe = {
        key: {
            publicKey: "pk_test_0000",
        },
        plans: {
            monthly: "CompanynameMonthly",
            yearly: "CompanynameYearly",
        }
    };
}

export class ResourceStrings {
    readonly errors = {
        cameraNotFound: 'Camera not found.',
        cameraNotOnline: 'Camera not online.',
        notReadyToPair: 'Camera is not ready to pair.',
        pairWindowExpired: 'Camera pairing window expired.',
        cameraNotOwned: 'Camera not owned.',
        targetUserNotFound: 'Target user not found.',
        cameraNotOwnedOrShared: 'Camera not owned or shared.',
        couldNotRevokeShare: 'Could not revoke camera sharing.',
        noNameFound: 'Name not found.',
        userUpdateRole: 'Must be admin or owner of own account to make changes.',
        userUpdateId: 'To update a user account a User ID is required.',
        userCannotHaveId: 'New User cannot have an ID',
        userPassOrIdMissing: 'User ID or password is missing',
        userRemove: 'To delete an account you must be admin, or the owner of the account',
        userReset: "Could not reset the password, email address not found",
        subRetrieval: 'Sorry we had a problem retrieving your subscription. Please try again!',
        subInactive: 'Sorry Your user account is inactive. Restart it <a href="/renew">here</a>',
        subEnded: 'Sorry Your subscription has finished. Restart it <a href="/renew">here</a>',
        subProblem: 'Sorry there is a problem with your subscription. Restart it <a href="/renew">here</a>',
        subExpired: 'Sorry Your subscription has expired. Restart it <a href="/renew">here</a>',
    }
    readonly storageKeys = {
        user: 'user',
    }
    readonly validCC = {
        broadcastStart: "broadcastStart",
        broadcastStop: "broadcastStop",
        pan: "pan",
        tilt: "tilt",
        zoom: "zoom",
    }
    readonly userCameraRel = {
        owned: 'Owned',
        shared: 'Shared',
    }
    readonly cameraStatus = {
        active: 'Online',
        inactive: 'Offline',
        pair: 'Ready to Pair',
    }
}

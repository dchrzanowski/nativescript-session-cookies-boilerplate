import { Injectable } from '@angular/core';
import * as utils from 'tns-core-modules/utils/utils';
import { isIOS, isAndroid } from "tns-core-modules/platform";
import * as frame from 'tns-core-modules/ui/frame';

@Injectable({
    providedIn: 'root'
})
export class UtilsService {

    constructor() { }

    /**
     * Dismiss the keyboard
     */
    dismissKeyboard() {
        if (isIOS) {
            frame.topmost().nativeView.endEditing(true);
        }
        if (isAndroid) {
            utils.ad.dismissSoftInput();
        }
    }
}

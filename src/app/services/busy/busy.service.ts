import { Injectable } from '@angular/core';
import { LoadingIndicator } from 'nativescript-loading-indicator';

@Injectable({
    providedIn: 'root'
})
export class BusyService {
    private loader: LoadingIndicator;
    private options = {
        message: 'Loading...',
        android: {
            margin: 20,
            dimBackground: true,
            color: "#FFFFFF", // color of indicator and labels
            hideBezel: true, // default false, can hide the surrounding bezel
        },
        ios: {
            margin: 20,
            dimBackground: true,
            color: "#FFFFFF", // color of indicator and labels
            hideBezel: true, // default false, can hide the surrounding bezel
        }
    };

    constructor() {
        this.loader = new LoadingIndicator();
    }

    /**
     * Show the Busy Indicator
     */
    show() {
        this.loader.show(this.options);
    }

    /**
     * Hide the Busy Indicator
     */
    hide() {
        this.loader.hide();
    }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../../shared/dto/user.dto';
import { ICardRenew } from '../../../shared/interfaces/card-renew.interface';
import { IStripeGenerateUserId } from '../../../shared/interfaces/generate-stripe-id.interface';
import { IUserLoginDetails } from '../../../shared/interfaces/user-login-details.interface';
import { IUserRegister } from '../../../shared/interfaces/user-register.interface';
import { ResourceService } from '../../resources/resources.service';

@Injectable({
    providedIn: 'root'
})
export class RestUserService {

    constructor(
        private readonly http: HttpClient,
        private readonly R: ResourceService,
    ) { }

    /**
     * Retrieve all users
     */
    find(): Observable<User[]> {
        return this.http
            .get<User[]>(this.R.settings.url.user.root)
    }

    /**
     * Retrieve a specific user
     * @param id ID of the user
     */
    findOne(id: number): Observable<User> {
        return this.http
            .get<User>(this.R.settings.url.user.root + id)
    }

    /**
     * Update a user
     * @param user User object
     */
    update(user: User): Observable<any> {
        return this.http
            .put<User>(this.R.settings.url.user.root, user)
    }

    /**
     * Remove a user
     * @param id ID of the user
     */
    remove(id: number) {
        return this.http
            .delete(this.R.settings.url.user.root + id)
    }

    /**
     * Register a new user
     * @param user User object
     */
    register(user: IUserRegister): Observable<any> {
        return this.http
            .post<User>(this.R.settings.url.user.register, user)
    }

    /**
     * Remove user's subscription
     */
    subRemove(): Observable<any> {
        return this.http
            .delete(this.R.settings.url.user.subscription)
    }

    /**
     * Provide a new card for the user's subscription
     */
    subRenew(cardRenew: ICardRenew): Observable<any> {
        return this.http
            .post(this.R.settings.url.user.subscription, cardRenew)
    }

    /**
     * Renew User's account
     * @param body Object necessary to renew account
     */
    renew(body: IStripeGenerateUserId) {
        return this.http
            .post(this.R.settings.url.user.renew, body)
    }

    /**
     * Login a user
     * @param userLoginDetails
     */
    login(userLoginDetails: IUserLoginDetails): Observable<any> {
        return this.http
            .post(this.R.settings.url.user.login, userLoginDetails);
    }

    /**
     * Logout a user
     */
    logout(): Observable<any> {
        return this.http
            .post(this.R.settings.url.user.logout, {})
    }

    /**
     * Request a password recovery
     * @param email Email address of the account that want to be recovered
     */
    recovery(email: string): Observable<any> {
        return this.http
            .post(this.R.settings.url.user.requestRecovery, { email: email })
    }

    /**
     * Ping the server to refresh the token
     */
    ping() {
        return this.http
            .get(this.R.settings.url.user.ping)
    }
}

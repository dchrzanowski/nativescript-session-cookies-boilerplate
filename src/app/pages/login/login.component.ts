import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { AuthService } from "~/app/services/auth/auth.service";
import { NotifyService } from "~/app/services/notify/notify.service";
import { RestUserService } from "~/app/services/rest/user/rest-user.service";
import { UtilsService } from "../../services/utils/utils.service";

@Component({
    selector: "Login",
    moduleId: module.id,
    templateUrl: "./login.component.html"
})
export class LoginComponent implements OnInit, AfterViewInit {
    @ViewChild('email') email: ElementRef;

    username: string;
    password: string;
    buttonLock: boolean = true;

    constructor(
        private readonly utils: UtilsService,
        private readonly auth: AuthService,
        private readonly notify: NotifyService,
        private readonly router: Router,
        private readonly user: RestUserService,
    ) { }

    ngOnInit(): void {
        // Init your component properties here.
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.email.nativeElement.focus();
        }, 600);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    /**
     * Submit details from the form for login purpose
     */
    async login() {
        this.utils.dismissKeyboard();
        this.lockButtons();

        try {
            await this.auth.login({
                username: this.username,
                password: this.password,
            })
            // notify
            this.notify.success(`Logged in as ${this.username}`);
            // redirect
            this.router.navigate(['/']);
        }
        catch {
            this.notify.error('Incorrect Credentials.');
        }
        this.unlockButtons();
    }

    /**
     * Submit details for password reset
     */
    async forgotPassword() {
        this.utils.dismissKeyboard();
        this.lockButtons();

        this.user.recovery(this.username)
            .subscribe(() => {
                this.notify.info(`A password reset link has been sent to ${this.username}`);
            }, (error) => {
                console.log("----- error ----\n", error);
                this.notify.somethingWrong();
            });

        this.unlockButtons();
    }

    /**
     * Lock buttons during payments processing
     */
    lockButtons() {
        this.buttonLock = false;
    }

    /**
     * Unlock buttons after processing
     */
    unlockButtons() {
        this.buttonLock = true;
    }
}

import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { AuthService } from "~/app/services/auth/auth.service";
import { NotifyService } from "~/app/services/notify/notify.service";

@Component({
    selector: "Logout",
    moduleId: module.id,
    templateUrl: "./logout.component.html"
})
export class LogoutComponent implements OnInit {

    constructor(
        private readonly auth: AuthService,
        private readonly router: Router,
        private readonly notify: NotifyService,
    ) {

    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    /**
     * Logout the user
     */
    async logout() {
        try {
            await this.auth.logout()
            this.router.navigate(['/']);
            this.notify.success("You have been logged out");
        } catch {
            this.notify.somethingWrong();
        }
    }

    /**
     * Leave the logout page and go home
     */
    leave() {
        this.router.navigate(['/']);
    }
}
